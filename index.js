/**
 * Задание 5 - Трансформеры и ООП
 * 
 * 1. Создать класс Transformer со свойствами name и health (по умолчанию
 *    имеет значение 100) и методом attack()
 * 2. Создать класс Autobot, который наследуется от класса Transformer.
 *    - Имеет свойсто weapon, т.к. автоботы сражаются с использованием оружия.
 *    - Конструктор класса принимает 2 параметра: имя и оружее (Экземпляр класса
 *      Weapon). 
 *    - Метод attack возвращает результат использования оружия weapon.fight()
 * 3. Создать класс Deceptikon, который наследуется от класса Transformer.
 *    - Десептиконы не пользуются оружием, поэтому у них нет свойства weapon. Зато
 *      они могут иметь разное количество здоровья.
 *    - Конструктор класса принимает 2 параметра: имя и количество здоровья health
 *    - Метод attack возвращает характеристики стандартного вооружения: { damage: 5, speed: 1000 }
 * 4. Создать класс оружия Weapon, на вход принимает 2 параметра: damage - урон 
 *    и speed - скорость атаки. Имеет 1 метод fight, который возвразает характеристики 
 *    оружия в виде { damage: 5, speed: 1000 }
 * 5. Создать 1 автобота с именем OptimusPrime с оружием, имеющим характеристики { damage: 100, speed: 1000 }
 * 6. Создать 1 десептикона с именем Megatron и показателем здоровья 10000
 * 7. Посмотреть что происходит при вызове метода atack() у траснформеров разного типа, 
 *    посмотреть сигнатуры классов
 * 
 * 8. ДЗ-Вопрос: Написаиь симуляцию. Сколько нужно автоботов чтобы победить Мегатрона если параметр speed в оружии это
 *    количество милсекунд до следующего удара?
 */
class  Transformer {
    constructor(name, health = 100) { //n-имя h-количество здоровья.
        this.name = name;
        this.health = health;
    }

    attack() {}

    hit(weapon){
        this.health = this.health - weapon.damage; //
    }


}

class Autobot extends Transformer { //n-имя w-оружие
    constructor(name, weapon) {
        super(name);
        this.weapon = weapon;
    }

    attack() {
        return this.weapon.fight();
    }
}

class Deceptikon extends Transformer {
    attack() {
        return {damage: 5, speed: 1000}; //d-урон s-скорость
    }
}

class Weapon {
    constructor(damage, speed) {
        this.damage = damage;
        this.speed = speed;
    }

    fight(){
        return{
            speed: this.speed,
            damage: this.damage

        }
    }
}

//const weapon = new Weapon(100, 1000);
//const optimus = new Autobot('OptimusPrime',weapon);
//const megatron = new Deceptikon('Megatron', 10000);

//optimus.hit(megatron.fight());


class Arena {
    constructor(side1, side2, visualizer) {
        this.side1 = side1;
        this.side2 = side2;
        this.visualizer = visualizer;
    }

    fight() {
        const timer = setInterval(
            () => {
                /**
                 * Шаг 1 - все стороны атакуют друг друга
                 * Шаг 2 - проверяем условие победы
                 * Шаг 3 - Отрисовка текущего состояния
                 */

// Сторона 1 атакует сторону 2
                this.side1.forEach(bot => this.side2[0].hit(bot.attack()));

// Проверяем условие победы стороны 1
                this.side2 = this.side2.filter(bot => bot.health > 0);
                if (!this.side2.length) {
                    console.log('Сторона 1 победила');
                    clearInterval(timer);
                }

// Сторона 2 атакует сторону 1
                this.side2.forEach(bot => this.side1[0].hit(bot.attack()));

// Проверяем условие победы стороны 2
                this.side1 = this.side1.filter(bot => bot.health > 0);
                if (!this.side1.length) {
                    console.log('Сторона 2 победила');
                    clearInterval(timer);
                }
                this.visualizer.render(
                    this.side1.map(bot => bot.health),
                    this.side2.map(bot => bot.health)
                );
            },
            100
        );
    }
}

class Visualizer {
    render(hps1, hps2) {
        const side1El = document.querySelector('.arena-side-1');
        const side2El = document.querySelector('.arena-side-2');

        side1El.innerHTML = hps1.map(hp => '<div class="bot"><span>' + hp + 'hp</span></div>').join('');
        side2El.innerHTML = hps2.map(hp => '<div class="bot"><span>' + hp + 'hp</span></div>').join('');
    }
}

const arena = new Arena (
    [
        new Autobot('OptimusPrime', new Weapon(100, 3000)),
        new Autobot('Autobot1', new Weapon(50, 2000)),
        new Autobot('Autobot2', new Weapon(50, 1000)),
        new Autobot('Autobot3', new Weapon(50, 1000)),


    ],
    [
        new Deceptikon('Megatron', 10000),

    ],
    new Visualizer()
    );

arena.fight();
